package Mongo;

import ExchangeClasses.Message;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@WebService(endpointInterface = "Mongo.MongoWS")
public class MongoWSImpl implements MongoWS {
    @Override
    public Message[] getAllMessages() {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(), fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoClient mongoClient = new MongoClient("10.0.0.4", MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build());
        MongoDatabase messagesDB = mongoClient.getDatabase("Messages");
        messagesDB.withCodecRegistry(pojoCodecRegistry);
        MongoCollection collection = messagesDB.getCollection("Message", Message.class);
        FindIterable<Message> resultSet = collection.find();
        List<Message> returnList = new ArrayList<>();
        for (Message msg : resultSet) {
            returnList.add(msg);
        }
        Message ret[] = new Message[returnList.size()];
        for (int i = 0; i < returnList.size(); i++) {
            ret[i] = returnList.get(i);
        }
        return ret;
    }
}
